/*
	Searching for SHA-1 partial collisions
	GBR
*/

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Random;
import java.util.HashMap;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.File;

import java.time.LocalDateTime;

class Collidedevlist
{
	private static File dumpFile;
	public static void main(String args[])
	{	
		HashMap<String, String> map = new HashMap<String, String>();

		ArrayList<String> hashes = new ArrayList<String>();
		ArrayList<String> strings = new ArrayList<String>();

		ArrayList<File> dumpFileList = new ArrayList<File>();
		
		long maxTrials = 5000000; // maximum number of trials before doing a dump and full comparison
		
		String[] charset = new String[] { 			
			"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
		};

		// Before we start, check that we can perform hashes using UTF-8
		try { sha1("HELLO"); } catch(Exception e) { System.out.println("Can't produce SHA-1 hashes with UTF-8 on this platform.\n"+e); return; }
					
		// Crunch time
		// Crunch as much as we can and compare the results to previously calculated tables on disk
		boolean success = false;
		long cycles = 0;

		try { success = crunch(hashes, strings, charset, maxTrials); } catch(Exception e) { System.out.println("Exception during crunch: " + e); return; }
		try { dump(hashes, strings, dumpFile = getNewDump()); } catch(Exception e) { System.out.println("Could not dump: " + e); return; }	
		hashes = new ArrayList<String>();
		strings = new ArrayList<String>();	
		if(success) { return; }

		try { load(dumpFile, hashes, strings); } catch(Exception e) { System.out.println("Could not load dump: " + e); return; }
	}	

	private static boolean crunch(ArrayList<String> hashes, ArrayList<String> strings, String[] charset, long stopAt) throws Exception
	{
		//
		// LET'S GET CRUNCHING
		//		
		//
		// DON'T STOP
		
		Random rand = new Random();
		boolean collide = false;
		byte[] hash = null;
		int stringLength = 20;
		int shaLength = 15;	// in nibbles
		long trials = 0;	
		long alreadyTried = 0;

		System.out.println("\nSHA-1 truncation length: " + shaLength + "\nCharacter set: " + charset.length + "\nString length: " + stringLength);

		String previousMap; // Store any previously mapped values for the key in here - that'd be the collision :D
		while(trials < 5000000)
		{	
			String s = generateString(stringLength, charset, rand);
			hash = sha1(s);
			String hexHash = getHex(hash);
			
			// now truncate so we only compare the bits we want
			hexHash = hexHash.substring(0,shaLength);

			hashes.add(hexHash);
			strings.add(s);
			
			trials++;
			if((trials % 1000) == 0) 
			{ 
				System.out.print("\rTrials: " + trials);
			}
		}
		
		return false;
	}

	private static File getNewDump() throws Exception
	{
		File f = null;	
		LocalDateTime dt = LocalDateTime.now();
		f = new File("collide-test-" + dt.toString() + ".dump");
		if(!f.createNewFile())
		{
			throw new Exception(f.getName() + "already exists.");
		}
		return f;
	}

	private static byte[] sha1(String inp) throws Exception
	{
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] toHash = inp.getBytes("UTF-8");
		return md.digest(toHash);
	}

	private static void dump(HashMap<String, String> map, File dump) throws Exception
	{
		FileOutputStream fout = new FileOutputStream(dump);
		ObjectOutputStream oout = new ObjectOutputStream(fout);
		oout.writeObject(map);
		oout.close();
		fout.close();
	}
	
	private static void dump(ArrayList<String> hashes, ArrayList<String> strings, File dump) throws Exception
	{
		FileOutputStream fout = new FileOutputStream(dump);
		ObjectOutputStream oout = new ObjectOutputStream(fout);
		oout.writeObject(hashes);
		oout.close();
		fout.close();
		
		File stringsFile = new File(dump.getName() + ".x");
		fout = new FileOutputStream(stringsFile);
		oout = new ObjectOutputStream(fout);
		oout.writeObject(strings);
		oout.close();
		fout.close();
	}
	
	private static HashMap<String, String> load(File dumpTo) throws Exception
	{
		FileInputStream fin = new FileInputStream(dumpTo); 
		ObjectInputStream oin = new ObjectInputStream(fin);
		HashMap<String, String> map = (HashMap<String, String>) oin.readObject(); // creates a compilation warning ; ;
		oin.close();
		fin.close();
		return map;
	}
	
	private static void load(File getFrom, ArrayList<String> hashes, ArrayList<String> strings) throws Exception
	{
		FileInputStream fin = new FileInputStream(getFrom); 
		ObjectInputStream oin = new ObjectInputStream(fin);
		hashes = (ArrayList<String>) oin.readObject(); // creates a compilation warning ; ;
		oin.close();
		fin.close();
		
		File stringsFile = new File(getFrom.getName() + ".x");
		fin = new FileInputStream(stringsFile); 
		oin = new ObjectInputStream(fin);
		strings = (ArrayList<String>) oin.readObject(); // creates a compilation warning ; ;
		oin.close();
		fin.close();
	}
	
	private static String generateString(int length, String[] charset, Random rand)
	{
		String gen = new String("");
		for(int i = 0; i < length; i++)
		{
			gen += charset[rand.nextInt(charset.length)];
		}
		return gen;
	}	

	// Sourced from stackoverflow - bytes to hexstring
	// https://stackoverflow.com/questions/4895523/java-string-to-sha1
	private static String getHex(byte[] hash)
	{
		String result = "";
		for (int i=0; i < hash.length; i++) 
		{
			result += Integer.toString( ( hash[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return result;
	}
}
