/*
	Searching for SHA-1 partial collisions
	GBR
*/

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.HashMap;
import java.util.Arrays;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.File;

import java.time.LocalDateTime;

class Collideinc
{
	private static File dumpFile;
	public static void main(String args[])
	{	
		HashMap<String, String> map = new HashMap<String, String>();

		//ArrayList<File> dumpFileList = new ArrayList<File>();
		
		long maxTrials = 15000000; // number of hashes to store
		long startAt = 0;
		int shaLength = 15; // in nibbles
		dumpFile = null;		
		boolean weLoadedAMap = false;

		// Before we start, check that we can perform hashes using UTF-8
		try { sha1("HELLO"); } catch(Exception e) { System.out.println("Can't produce SHA-1 hashes with UTF-8 on this platform.\n"+e); return; }

		if(args.length == 1 && args[0].equals("-help"))
		{
			System.out.println("Collideinc - incrementing SHA-1 partial collision finder.\n\n-load\t\tLoad .dump file in CWD as reference hashmap.\n-rv\t\tInitial value for hash creation. Default: 0.\n-mapsize\tNumber of hashes to store in hashmap for comparison. \n\t\tDefault: 15,000,000. Large values will OOM.\n-shalength\tTruncation length of calculated SHA-1 hash in 4-bit\n\t\tincrements from most significant bit. Default: 15.\n");
			System.out.println("Note that false collisions will be found if iterating over values already stored\nin a loaded hashmap.\n");
			return; 
		}
		
		System.out.println("Collideinc - incrementing SHA-1 partial collision finder.");		
		
		if(args.length != 0)
		{
			int argsptr = 0;
			
			while(argsptr < args.length)
			{
				if(args[argsptr].equals("-load")) // Load existing hashmap and continue. Useful to continue from a "stored state" when combined with -rv equal to the exit point of previous runs.
				{
					System.out.print("-load: Selecting first available dump as resume..."); 
					try { dumpFile = findDump(); } catch(Exception e) { System.out.println("Cannot find dump file: " + e); return; }
					try { map = load(dumpFile); } catch(Exception e) { System.out.println("Cannot load dump file: " + e); return; }
					System.out.println("done.\n-load: [" + dumpFile.getName() + "].");
					weLoadedAMap = true;
				}
				else if(args[argsptr].equals("-mapsize"))
				{
					long l = 0;
					try { l = Long.parseLong(args[argsptr+1]); } catch(Exception e) { System.out.println("Invalid mapsize: " + args[argsptr+1]); return; }
					maxTrials = l;
					System.out.println("-mapsize: Storing " + l + " trials in hashmap.");
					argsptr++;
				}
				else if(args[argsptr].equals("-rv"))
				{
					try { startAt = Long.parseLong(args[argsptr+1]); } catch(Exception e) {  System.out.println("Invalid restart value: " + args[argsptr+1] + "\n" + e); return; }
					System.out.println("-rv: Restarting search at " + startAt + ".");
					argsptr++;
				}			
				else if(args[argsptr].equals("-shalength"))
				{
					try 
					{ 
						shaLength = Integer.parseInt(args[argsptr+1]);
						if(shaLength < 1 || shaLength > 15) { throw new Exception("Truncation length must be 0 < l < 16"); } 
					} 
					catch(Exception e) {  System.out.println("Invalid SHA-1 truncation length: " + args[argsptr+1] + "\n" + e); return; }
					System.out.println("-shalength: Truncating SHA-1 hash after " + shaLength + " nibbles (" + (shaLength*4) + " bits.)");
					argsptr++;
				}
				argsptr++;
			}
		}											
							
		System.out.println("\nSHA-1 truncation length: " + shaLength + "; starting at " + startAt + ".\nMap size: " + maxTrials +".");
		
		// Crunch time
		// Crunch as much as we can and compare the results to previously calculated tables on disk	
		try { crunch(map, maxTrials, startAt, shaLength, weLoadedAMap); } catch (Exception e) { System.out.println("Crunch error:" + e ); return; }			
	}	

	private static boolean crunch(HashMap<String, String> map, long maxTrials, long start, int shaLength, boolean weLoadedAMap) throws Exception
	{
		//
		// LET'S GET CRUNCHING
		//		
		
		String[] spinner = new String[] { 
			"|", "/", "-", "\\" 
		};
		//"
		
		int spinnerState = 0;
		int progressSpinner = 0;

		long trials = 0;
		boolean halted = weLoadedAMap;
		
		byte[] hash = null;	

		Long currentInput = Long.valueOf(start);
		
		while(true)
		{	
			String s = currentInput.toString();

			// calculate hash and truncate so we only compare the bits we want
			String hexHash = getHex(sha1(s)).substring(0,shaLength);
	
			// See if it's already in the list 
			if(map.containsKey(hexHash))
			{			
				// if the hash is already in there it MUST be a collision as we can never put the same string in twice.
				// This assumes we haven't loaded a hash table that contains values we're iterating over...
				System.out.println("\nCollision found on trial " + trials + " (" + (trials + start) + "): \n" + s + " [" + hexHash + "]\n" + map.get(hexHash) + " [" + hexHash + "]");
				return true;
			}
			
			// if we've not already generated our entire map, put this in
			else
			{
				if(!halted) {  map.put(hexHash, s); }
			}
			
			trials++;
			progressSpinner++;

			if(progressSpinner % 100000 == 0) 
			{ 
				progressSpinner = 0;
				
				System.out.print("\r[" + spinner[spinnerState] + "] ");
				if(!halted) { System.out.print(trials + "/" + maxTrials); }
				else	{ System.out.print(trials + " trials performed (-rv: " + (trials+start) + ")"); }
				
				if(spinnerState == 3) { spinnerState = 0; } else { spinnerState++; }
			}
			
			
			if(!halted && (trials == maxTrials)) 
			{ 
				halted = true;
				System.out.print("\rHashmap filled. Dumping resume point...");
				File f = getNewDump();				
				dump(map, f);
				System.out.println("done. \n[" + f.getName() + "]");
			}
			
			// increment
			currentInput++;
		}
	}

	private static File findDump() throws Exception
	{
		File enumerator = new File(".");
		File[] fl = enumerator.listFiles();
		for(int i = 0; i < fl.length; i++)
		{
			if(fl[i].getName().endsWith(".dump")) { return fl[i]; }
		}
		throw new Exception("No valid dump files found.");
	}

	private static File getNewDump() throws Exception
	{
		File f = null;	
		LocalDateTime dt = LocalDateTime.now();
		f = new File("collide-test-" + dt.toString() + ".dump");
		if(!f.createNewFile())
		{
			throw new Exception(f.getName() + "already exists.");
		}
		return f;
	}

	private static byte[] sha1(String inp) throws Exception
	{
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] toHash = inp.getBytes("UTF-8");
		return md.digest(toHash);
	}

	private static void dump(HashMap<String, String> map, File dump) throws Exception
	{
		FileOutputStream fout = new FileOutputStream(dump);
		ObjectOutputStream oout = new ObjectOutputStream(fout);
		oout.writeObject(map);
		oout.close();
		fout.close();
	}
	
	private static HashMap<String, String> load(File dumpTo) throws Exception
	{
		FileInputStream fin = new FileInputStream(dumpTo); 
		ObjectInputStream oin = new ObjectInputStream(fin);
		HashMap<String, String> map = (HashMap<String, String>) oin.readObject(); // creates a compilation warning ; ;
		oin.close();
		fin.close();
		return map;
	}
	
	// Sourced from stackoverflow - bytes to hexstring
	// https://stackoverflow.com/questions/4895523/java-string-to-sha1
	private static String getHex(byte[] hash)
	{
		String result = "";
		for (int i=0; i < hash.length; i++) 
		{
			result += Integer.toString( ( hash[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return result;
	}
}
