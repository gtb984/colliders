/*
	Searching for SHA-1 partial collisions
	GBR
*/

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Random;
import java.util.HashMap;
import java.util.Arrays;

class Collide 
{
	public static void main(String args[])
	{	

		// Hash-string pairs are stored here.
		HashMap<String, String> map = new HashMap<String, String>();
		
		// Set of characters that we can build strings from
		String[] charset = new String[] { 			
			"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
		};
		
		Random rand = new Random();
		boolean collide = false;
		byte[] hash = null;
		int stringLength = 25; // length of the strings we build.
		int shaLength = 15;	// in nibbles; 15 = 60 bits. Change this to a lower value to test if collisions are found by this algorithm.
		double trials = 0;		
		
		System.out.println("\nSHA-1 truncation length: " + shaLength + "\nCharacter set: " + charset.length + "\nString length: " + stringLength + "\nSet of strings: " + Math.pow((double) stringLength, (double) charset.length));	

		// While no collisions have been found, keep looping
		while(!collide)
		{	
			// Generate a random string and hash it. The exception is a character encoding issue that won't actually occur.
			String s = generateString(stringLength, charset, rand);
			try { hash = sha1(s); } catch(Exception e) { System.out.println("Exception: " + e); return; }

			// If the map already contains our generated string, generate new ones until we get a unique one.
			// The same string will give the same hash, and we don't want to confuse things.
			while(map.containsValue(s))
			{
				System.out.println("Already tried " + s);
				s = generateString(stringLength, charset, rand);
				try { hash = sha1(s); } catch(Exception e) { System.out.println("Exception: " + e); return; }

			}			
			
			// convert the bytes array of the hash into a hex string
			String hexHash = getHex(hash);
			
			// now truncate so we only compare the bits we want
			hexHash = hexHash.substring(0,shaLength);

			// If the map contains the hash already, it's a collision and we can all go home!
			if(map.containsKey(hexHash))
			{
				System.out.println("Collision found: \n" + s + " [" + hexHash + "]\n" + map.get(hexHash) + " [" + hexHash + "]");
				collide = true;
			}

			// Add the hash-string pair to the map and loop again
			map.put(hexHash, s);	
			trials++;
			if(trials % 10000 == 0) { System.out.println("Trials: " + trials); }			
		}
	}	

	/**
	*	Generates a SHA-1 hash of the input string.
	*/
	public static byte[] sha1(String inp) throws Exception
	{
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] toHash = inp.getBytes("UTF-8");
		return md.digest(toHash);
	}

	/**
	*	Generates a random string of a length we specify using the character set of our choice.
	*/
	public static String generateString(int length, String[] charset, Random rand)
	{
		String gen = new String("");
		for(int i = 0; i < length; i++)
		{
			gen += charset[rand.nextInt(charset.length)];
		}
		return gen;
	}
	
	// Sourced from stackoverflow - bytes to hexstring
	// https://stackoverflow.com/questions/4895523/java-string-to-sha1
	public static String getHex(byte[] hash)
	{
		String result = "";
		for (int i=0; i < hash.length; i++) 
		{
			result += Integer.toString( ( hash[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return result;
	}
}
