/*
	Searching for SHA-1 partial collisions
	GBR
*/

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Random;
import java.util.HashMap;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Set;
import java.util.Iterator;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.File;

import java.time.LocalDateTime;

class Collidedev
{
	// The file we'll dump to once our hashmap is full
	// A new one is created with every iteration
	private static File dumpFile;

	public static void main(String args[])
	{	
		// Hash-string pairs are stored here.
		HashMap<String, String> map = new HashMap<String, String>();

		// A list of the files we're comparing with. Add each new dump we create to this.
		ArrayList<File> dumpFileList = new ArrayList<File>();
		
 		// default maximum number of trials before doing a dump and full comparison
		// Too big and we may OOM when loading other maps to compare - this should be ~half our maximum capacity.
		long maxTrials = 5000000;
		
		// Set of characters that we can build strings from
		String[] charset = new String[] { 			
			"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
		};

		// Before we start, check that we can perform hashes using UTF-8
		try { sha1("HELLO"); } catch(Exception e) { System.out.println("Can't produce SHA-1 hashes with UTF-8 on this platform.\n"+e); return; }

		// Check command line arguments - resuming from existing dump files, and choosing the size of our hash map before dumping
		if(args.length != 0)
		{
			int argsptr = 0;
			
			while(argsptr < args.length)
			{
				if(args[argsptr].equals("-l"))
				{
					System.out.print("Loading existing dumps...");
					try { dumpFileList = enumerateDumps(); } catch(Exception e) { System.out.println("Cannot enumerate dump files: " + e); return; }
					System.out.println("done. Current file list: ");
					for(int i = 0; i < dumpFileList.size(); i++) { System.out.println(dumpFileList.get(i).getName()); }
				}
				else if(args[argsptr].equals("-i"))
				{
					long l = 0;
					try { l = Long.parseLong(args[argsptr+1]); } catch(Exception e) { System.out.println("Invalid argument: -i " + args[argsptr+1]); return; }
					maxTrials = l;
					System.out.println("Performing " + l + " trials in each cycle.");
					argsptr++;
				}
				argsptr++;
			}
		}											
							

		// Crunch time
		// Crunch as much as we can and compare the results to previously calculated tables on disk
		boolean success = false;
		long cycles = 0;
		
		while(!success)
		{
			// create a new dump	
			try { dumpFile = getNewDump(); }
			catch(Exception e) { System.out.println("Could not generate a new dump: " + dumpFile.getName() + "\n" + e); return; }
		
			System.out.println("Using " + dumpFile.getName() + " as new dump target.");
			
			// Crunch a new map
			try { success = crunch(map, charset, maxTrials); } catch(Exception e) { System.out.println("Exception during crunch: " + e); return; }
			if(success) { return; }

			System.out.println("Map generated.");
			
			// Compare it to existing files
			try { success = compareWithDumps(map, dumpFileList); } catch(Exception e) { System.out.println("Exception during compareWithDumps: " + e); return; }

			// spit out our new map and add it to the list to compare future ones with
			System.out.print("Dumping: " + dumpFile.getName() + "...");
			try { dump(map, dumpFile); } catch(Exception e) { System.out.println("Exception when dumping: " + e); return; }
			System.out.println("done.");
			
			dumpFileList.add(dumpFile);
			System.out.println("Current file list: ");
			for(int i = 0; i < dumpFileList.size(); i++) { System.out.println(dumpFileList.get(i).getName()); }

			// clear the map
			map = new HashMap<String, String>();
			
			cycles++;
			System.out.println("Completed cycle " + cycles);
		}	
	}	

	private static boolean crunch(HashMap<String, String> map, String[] charset, long stopAt) throws Exception
	{
		//
		// LET'S GET CRUNCHING
		//		
		
		Random rand = new Random();
		boolean collide = false;
		byte[] hash = null;
		int stringLength = 20; // length of the strings we build.
		int shaLength = 15;	// in nibbles
		long trials = 0;	
		long alreadyTried = 0;
		int progress = 1;

		System.out.println("\nSHA-1 truncation length: " + shaLength + "\nCharacter set: " + charset.length + "\nString length: " + stringLength);
		System.out.print("[                                        ] 0");

		String previousMap; // Store any previously mapped values for the key in here - that'd be the collision :D
		while(trials < stopAt)
		{	
			// generate the string and the hash
			String s = generateString(stringLength, charset, rand);
			hash = sha1(s);
			String hexHash = getHex(hash);
			
			// now truncate so we only compare the bits we want
			hexHash = hexHash.substring(0,shaLength);

			// Bung it in the map - if it's a duplicate hash, we'll know, and if it's a duplicate key too (already tried the key), we'll also know.
			previousMap = map.put(hexHash, s);
			if(previousMap!=null)
			{				
				if(!previousMap.equals(s))
				{
					System.out.println("\nCollision found on trial " + trials + ": \n" + s + " [" + hexHash + "]\n" + previousMap + " [" + hexHash + "]");
					return true;
				}
				// else we just tried to put the same thing in that was already there - false alarm
				else
				{
					alreadyTried++;
				}
			}
			trials++;

			// show our progress so it doesn't look like we've crashed or OOMed and no-one noticed
			if(trials == (long) (stopAt * (0.025 * progress))) 
			{ 
				String progressBar = "";
				int i = 0;
				for(i = 0; i < progress; i++)
				{
						progressBar = progressBar + "=";
				}
				for(; i < 40; i++)
				{
						progressBar = progressBar + " ";
				}
				System.out.print("\r[" + progressBar + "] " + trials);
				progress++;
			}
		}
		System.out.println();
		if(alreadyTried != 0) { System.out.println("Attempted to insert " + alreadyTried + "duplicate(s)."); }
		return false;
	}

	// Compare our current map with all the dumps we've created or been told to load
	private static boolean compareWithDumps(HashMap<String, String> currentMap, ArrayList<File> dumpFileList) throws Exception
	{
		HashMap<String, String> loadedMap = null;
		Set<String> hashes = currentMap.keySet();
		
		// iterate over files
		for(int i = 0; i < dumpFileList.size(); i++)
		{
			File currentFile = dumpFileList.get(i);
			System.out.print("Loading " + currentFile.getName() + "...");
			loadedMap = load(currentFile);
			System.out.print("done.\nComparing");
			Iterator<String> checkThisHash = hashes.iterator();
			int cycles = 0;

			// iterate over the keys of our new hashmap (the hashes) and see if they're in the ones in the dumped file 
			while(checkThisHash.hasNext())
			{
				String currentHash = checkThisHash.next();
				if(loadedMap.containsKey(currentHash))
				{
					// a hash matches - but are they from the same string?
					String currentStr = currentMap.get(currentHash);
					String loadedStr = loadedMap.get(currentHash);
					if(!currentStr.equals(loadedStr))
					{
						System.out.println("\nMatch from file " + currentFile.getName() + "\n" + currentStr + " [" + currentHash + "] :: " + loadedStr + " [ " + currentHash);						
						return true;
					}
					else
					{
						System.out.println("\nDuplicate in file " + currentFile.getName() + "; removing " + currentStr + " = " + loadedStr + " (" + currentHash + ")");
						// we've already done this hash - remove it so it's not stored in a new file.
						currentMap.remove(currentHash);
					}
				}
				cycles++;
				if(cycles % 100000 == 0) { System.out.print("."); }
			}
			loadedMap = null;
			checkThisHash = null;
			System.out.println();
		}
		System.out.println("No matches in " + dumpFileList.size() + " files.");
		hashes = null;
		return false;
	}			

	// this could probably be more elegant
	// enumerate all files in the cwd and try to load the ones with the .dump extension
	private static ArrayList<File> enumerateDumps() throws Exception
	{
		File enumerator = new File(".");
		File[] fl = enumerator.listFiles();
		ArrayList<File> l = new ArrayList<File>();
		for(int i = 0; i < fl.length; i++)
		{
			if(fl[i].getName().endsWith(".dump")) {	l.add(fl[i]); }
		}
		return l;
	}

	// generate a new dump file from the current time - should be unique.
	private static File getNewDump() throws Exception
	{
		File f = null;	
		LocalDateTime dt = LocalDateTime.now();
		f = new File("collide-test-" + dt.toString() + ".dump");
		if(!f.createNewFile())
		{
			throw new Exception(f.getName() + "already exists.");
		}
		return f;
	}

	/**
	*	Generates a SHA-1 hash of the input string.
	*/
	private static byte[] sha1(String inp) throws Exception
	{
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] toHash = inp.getBytes("UTF-8");
		return md.digest(toHash);
	}

	// Dump this map to this file
	private static void dump(HashMap<String, String> map, File dump) throws Exception
	{
		FileOutputStream fout = new FileOutputStream(dump);
		ObjectOutputStream oout = new ObjectOutputStream(fout);
		oout.writeObject(map);
		oout.close();
		fout.close();
	}
	
	// Load this dump into a map and return it
	private static HashMap<String, String> load(File dumpTo) throws Exception
	{
		FileInputStream fin = new FileInputStream(dumpTo); 
		ObjectInputStream oin = new ObjectInputStream(fin);
		HashMap<String, String> map = (HashMap<String, String>) oin.readObject(); // creates a compilation warning ; ;
		oin.close();
		fin.close();
		return map;
	}

	/**
	*	Generates a random string of a length we specify using the character set of our choice.
	*/
	private static String generateString(int length, String[] charset, Random rand)
	{
		String gen = new String("");
		for(int i = 0; i < length; i++)
		{
			gen += charset[rand.nextInt(charset.length)];
		}
		return gen;
	}	

	// Sourced from stackoverflow - bytes to hexstring
	// https://stackoverflow.com/questions/4895523/java-string-to-sha1
	private static String getHex(byte[] hash)
	{
		String result = "";
		for (int i=0; i < hash.length; i++) 
		{
			result += Integer.toString( ( hash[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return result;
	}
}
