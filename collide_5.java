/*
	Searching for SHA-1 partial collisions
	GBR
*/

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import java.util.Random;
import java.util.HashMap;
import java.util.Arrays;

class Collidehalt
{
	public static void main(String args[])
	{	
		HashMap<String, String> map = new HashMap<String, String>();
		
		String[] charset = new String[] { 			
			"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
			"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
			"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
		};
		
		long storedHashes = 10000000; // default size of hashmap storing hash-string pairs
		
		// pass hashmap size via command line
		if(args.length != 0)
		{
			try { storedHashes = Long.parseLong(args[0]); } catch(Exception e) { System.out.println("Invalid argument for number of stored hashes: " + args[0] + "\n" + e); return; }
		}

		// Before we start, check that we can perform hashes using UTF-8
		try { sha1("HELLO"); } catch(Exception e) { System.out.println("Can't produce SHA-1 hashes with UTF-8 on this platform.\n"+e); return; }

		try { crunch(map, charset, storedHashes); } catch (Exception e) { System.out.println("Error: " + e); return; }
		
	}	

	private static boolean crunch(HashMap<String, String> map, String[] charset, long stopAt) throws Exception
	{
		//
		// LET'S GET CRUNCHING
		//		
		
		String[] spinner = new String[] { 
			"|", "/", "-", "\\" 
		};//"

		Random rand = new Random();
		boolean collide = false;
		byte[] hash = null;
		int stringLength = 20;
		int shaLength = 15;	// in nibbles

		long trials = 0;	
		boolean halted = false;

		// progress bar stuff
		long progressSpinner = 0;
		int spinnerState = 0;

		System.out.println("\nSHA-1 truncation length: " + shaLength + "\nCharacter set: " + charset.length + "\nString length: " + stringLength + "\nStoring " + stopAt + " hashes only.");

		while(true)
		{	
			// build the string and hash it
			String s = generateString(stringLength, charset, rand);
			hash = sha1(s);
			String hexHash = getHex(hash);
			
			// now truncate so we only compare the bits we want
			hexHash = hexHash.substring(0,shaLength);

			// Bung it in the map - if it's a duplicate hash, we'll know, and if it's a duplicate key too (already tried the key), we'll also know.
			boolean containsKey = map.containsKey(hexHash);			
			if(containsKey)
			{
				String found = map.get(hexHash);
				if(found.equals(s))
				{
					System.out.println("\rAlready tried " + s);
				}
				else
				{			
					System.out.println("\nCollision found on trial " + trials + ": \n" + s + " [" + hexHash + "]\n" + found + " [" + hexHash + "]");
				}
			}
			else
			{
				if(!halted) { map.put(hexHash, s); }
			}

			progressSpinner++;
			if(!halted) { trials++; }

			// progress bar so we know how far we've got
			if(progressSpinner % 100000 == 0) 
			{ 
				progressSpinner = 0;
				System.out.print("\r[" + spinner[spinnerState] + "] ");
				if(!halted) { System.out.print(trials + "/" + stopAt); }
				if(spinnerState == 3) { spinnerState = 0; } else { spinnerState++; }
			}

			if(trials == stopAt && !halted) { System.out.println("\rReached cap. (" + trials + "/" + trials + ")"); halted = true; }


		}
	}

	private static byte[] sha1(String inp) throws Exception
	{
		MessageDigest md = MessageDigest.getInstance("SHA-1");
		byte[] toHash = inp.getBytes("UTF-8");
		return md.digest(toHash);
	}

	private static String generateString(int length, String[] charset, Random rand)
	{
		String gen = new String("");
		for(int i = 0; i < length; i++)
		{
			gen += charset[rand.nextInt(charset.length)];
		}
		return gen;
	}

	// Sourced from stackoverflow - bytes to hexstring
	// https://stackoverflow.com/questions/4895523/java-string-to-sha1
	private static String getHex(byte[] hash)
	{
		String result = "";
		for (int i=0; i < hash.length; i++) 
		{
			result += Integer.toString( ( hash[i] & 0xff ) + 0x100, 16).substring( 1 );
		}
		return result;
	}
}
